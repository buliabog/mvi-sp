# MVI Sp


## The task

I took the task from [Kaggle](https://www.kaggle.com/competitions/novozymes-enzyme-stability-prediction/overview)

The essence of task was Assessment of enzyme stability.

Enzymes are chains of amino acids.

They are widely used in various fields, starting with industrial production (as catalysts), ending with medicine.

The problem is that they are not very stable. In relatively hight temperature they are destroyed, disintegrating into useless fragments. On the other hand the processes which enzymes shall speed up, will just slow down under low temperature. So we will need much more catalyst to get the effect. Enzymes don't destroy instantly, but the longer it lives the less enzyme we need, and the cheaper it is. 

The goal of the study was to create a tool for effective assessment of enzyme stability base on its formula. Contest participants should sort test data as accurately as possible.

## Files

We have here files and directories:
- datasets/test.csv - data for assessment, to send it as result for contest.
- datasets/train.csv - data for training of models. I load here just small part, cause it is the property of other company.
- datasets/sample_submission.csv - example of Submission file for contest.
- model *** - directories with my models and its results.
- [report.pdf](https://gitlab.fit.cvut.cz/buliabog/mvi-sp/-/blob/master/report.pdf) - the report about my semester work.
- Předzpracování_semestrálky.docx - not actual now. There was my first milestone. 
- [Buliakov_MVI_test_result.bmp](https://gitlab.fit.cvut.cz/buliabog/mvi-sp/-/blob/master/Buliakov_MVI_test_result.bmp) - results of test on Deep Learning Quiz. 

In every directory "model ***" are files:
- ***.ipynb - the program.
- learning_logs.log - file with logs of model learning process.

For models 4 and 5 there are more log files:
- for Clasification there are 4 files - 1 for clasificator, 3 for solvers for different classes.
- for RGBoost there are 3 files  for 3 steps of result improvement.

I have also .h5 files for every model on every epoch I have reached, but they are not usefull without *tf.keras.models.load_model*, so I have decided not to add them into repository. Probably it will just take disk space for nothing.


## Program start

I have written scripts as Google colab notebooks. 

You can start the programs in 2 ways:
1) Locally [on your PC](https://research.google.com/colaboratory/local-runtimes.html)
2) On Google colab servers.

Also you should check the source with datasets. Use for this the Variable DATA_SOURCES:
1) Kaggle
2) GDrive
3) Local

Since you have no access to Kaggle datasets, you will get data from Gitlab repository. So, please use options 2 or 3.

You will have to check and probably change WORK_DIREKTORY in scripts to use correct directory from your actual PC or server.

## Structure of Scripts

My programms have this structure:
1) **Config** - here we set some variables and do some imports for python
2) **Work with datasets** - download data from chosen source, set Variable WORK_DIREKTORY
3) **Research** - we look at data here
4) **Convolution** - in final scripts, I create alphabet for our enzymes here
5) **Time for neuro net** - creation of function NovozymesPreprocess for preprocessing and neural net
6) **Process Test data for Prediction** - form data for contest to load to Kaggle
7) **Draw coefficients** - actual only for model 1


